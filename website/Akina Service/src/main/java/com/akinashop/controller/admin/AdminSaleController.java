package com.akinashop.controller.admin;

import com.akinashop.entities.Sale;
import com.akinashop.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdminSaleController {
    @Autowired
    private SaleService saleService;

    @GetMapping(value = "/sales")
    public List<Sale> findAllSale(){
        return saleService.findAllSale();
    }

    @PostMapping(value = "/save-sale")
    public Sale saveSale(@RequestBody Sale sale){
        return saleService.saveSale(sale);
    }

    @DeleteMapping(value = "/delete-sale")
    public void deleteSale(@RequestParam int id){
        saleService.deleteSale(id);
    }

    @GetMapping(value = "/code-sale")
    public Sale findPriceByCode(@RequestParam String nameSale){
        return saleService.findPriceByCode(nameSale);
    }

}
