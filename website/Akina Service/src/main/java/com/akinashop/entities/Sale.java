package com.akinashop.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tbl_sale")
public class Sale extends BaseEntity{
    @Column(name = "name", nullable = false)
    private String nameSale;

    @Column(name = "price", nullable = false)
    private BigDecimal priceSale;

    @Column(name = "time", nullable = false)
    private int timeSale;

    @Column(name = "quantity")
    private int quantity;

    public String getNameSale() {
        return nameSale;
    }

    public void setNameSale(String nameSale) {
        this.nameSale = nameSale;
    }

    public BigDecimal getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(BigDecimal priceSale) {
        this.priceSale = priceSale;
    }

    public int getTimeSale() {
        return timeSale;
    }

    public void setTimeSale(int timeSale) {
        this.timeSale = timeSale;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
