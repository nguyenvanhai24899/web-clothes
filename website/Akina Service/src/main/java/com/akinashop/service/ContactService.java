package com.akinashop.service;

import com.akinashop.entities.Contact;

import java.util.List;

public interface ContactService {
    Contact saveContact(Contact contact);
    List<Contact> findAllContact();
    void deleteContact(int id);
}
