package com.akinashop.service;

import com.akinashop.entities.Sale;

import java.util.List;

public interface SaleService {
    List<Sale> findAllSale();
    Sale saveSale(Sale sale);
    void deleteSale(int id);
    Sale findPriceByCode(String nameSale);
}
