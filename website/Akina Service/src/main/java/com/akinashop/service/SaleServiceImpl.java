package com.akinashop.service;

import com.akinashop.entities.Sale;
import com.akinashop.repositories.SaleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService{
    @Autowired
    private SaleRepo saleRepo;


    @Override
    public List<Sale> findAllSale() {
        return saleRepo.findAll();
    }

    @Override
    public Sale saveSale(Sale sale) {
        return saleRepo.save(sale);
    }

    @Override
    public void deleteSale(int id) {
        saleRepo.deleteById(id);
    }

    @Override
    public Sale findPriceByCode(String nameSale) {
       return saleRepo.findPriceByCode(nameSale);
    }

}
