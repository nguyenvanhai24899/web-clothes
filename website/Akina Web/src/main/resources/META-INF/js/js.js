var Shop = {
    addItemToCart: function(productId, quantity) {
        var data = {
            "productId": productId,
            "quantity": quantity
        };
        $.ajax({
            url: "/add-to-cart",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(data),

            dataType: "json",
            success: function(jsonResult) {
                $("#btnCheckout").html("Giỏ hàng("+jsonResult.data+")");
                $('html, body').animate({
                    scrollTop: $("#btnCheckout").offset().top - 100
                }, 1000);
            }
        });
    },
}