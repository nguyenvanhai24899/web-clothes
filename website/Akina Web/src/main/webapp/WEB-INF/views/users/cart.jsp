<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- SPRING FORM -->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fashi | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <jsp:include page="/WEB-INF/views/users/common/css.jsp"></jsp:include>

</head>

<body>
<%--header--%>
<jsp:include page="/WEB-INF/views/users/common/header.jsp"></jsp:include>

<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section" style="margin-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-text product-more">
                    <a href="./home.html"><i class="fa fa-home"></i> Trang chủ</a>
                    <a href="./shop.html">Bộ sưu tập</a>
                    <span>Giỏ hàng</span>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="shopping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-table">

                    <table>
                        <thead>
                            <tr>
                                <th>Ảnh sản phẩm</th>
                                <th class="p-name">Tên sản phẩm</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                                <th>Thành tiền</th>
                                <th onclick="delteALlItems()"><i class="ti-close"></i></th>
                            </tr>
                        </thead>
                        <tbody class="cart-items">
                            <c:forEach items="${GIO_HANG.cartItems}" var="item">
                                <tr class="cart-item">
                                    <td class="cart-pic"><img src="img/cart-page/product-3.jpg" alt=""></td>
                                    <td class="cart-title">
                                        <h5>${item.productName}</h5>
                                    </td>
                                    <td class="p-price">
                                    <span class="price">
                                        ${item.unitPrice}

<%--                                        <fmt:formatNumber value="${item.unitPrice}" type="CURRENCY"/>--%>
                                    </span>
                                    </td>
                                    <td class="qua-col">
                                        <div class="wrap-num-product flex-w m-auto">
                                            <div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                                                <i class="fa fa-minus"></i>
                                            </div>
                                            <input class="mtext-104 cl3 txt-center num-product quantity" type="number" name="num-product" min="1" max="5" value="1">
                                            <div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                                                <i class="fa fa-plus"></i>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="total-price">
                                        <span class="total-pr">
                                            ${item.unitPrice * item.quantity}
<%--                                        <fmt:setLocale value="vi_VN"/>--%>
<%--                                        <fmt:formatNumber value="${item.unitPrice * item.quantity}" type="CURRENCY"/>--%>
                                        </span>
                                    </td>
                                    <td class="close-td" onclick="delteItem()"><a ><i class="ti-close"></i></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                 </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="discount-coupon">
                            <h6>Mã giảm giá</h6>
                            <form  class="coupon-form">
                                <input type="text" placeholder="Nhập mã giảm giá..." id="code-sale">
                               <button type="submit" class="site-btn coupon-btn" id="add-code">Thêm</button>
                            </form>

                        </div>
                    </div>
                    <div class="col-lg-4 offset-lg-4">
                        <div class="proceed-checkout">
                            <ul>
                                <li class="subtotal">Tổng phụ <span class="total-order">${totalOrder} đ</span></li>
                                <li class="subtotal">Mã giảm giá <span id="price-sale">${sale} đ</span></li>
                                <li class="cart-total">Thành tiền <span>
                                </span></li>
                            </ul>
                            <a href="#" class="proceed-btn">Tiến hành thanh toán</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Footer Section Begin -->
<jsp:include page="/WEB-INF/views/users/common/footer.jsp"></jsp:include>


<jsp:include page="/WEB-INF/views/users/common/js.jsp"></jsp:include>

<script>
    $('#add-code').click(function(e) {
        e.preventDefault();
        let nameSale = $('#code-sale').val();
        $.ajax({
            type: "GET",
            url: "/code-sale/",
            data: {
                nameSale: nameSale
            },
            success: function(result) {
                //$('#price-sale').text(re);
                alert('ok');
            },
            error: function(result) {
                alert('error');
            }
        });
    });
</script>
</body>
</html>

