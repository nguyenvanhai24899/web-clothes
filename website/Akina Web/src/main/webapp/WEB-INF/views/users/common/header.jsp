<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<header>
    <!-- Header desktop -->
    <div class="container-menu-desktop">
        <!-- Topbar -->
        <div class="top-bar" >
            <div class="content-topbar flex-sb-m h-full container">
                <div class="left-top-bar" style="font-size: 1rem;">
                    HotLine: 0999888777
                </div>

                <div class="right-top-bar flex-w h-full" >
                    <a href="#" class="flex-c-m trans-04 p-lr-25" style="font-size: 1rem;">
                        <i class="fa fa-user"></i>&ensp;Đăng nhập
                    </a>

                    <a href="#" class="flex-c-m trans-04 p-lr-25" style="font-size: 1rem;">
                        <i class="fa fa-sign-out"></i>&ensp;Đăng ký
                    </a>
                </div>
            </div>
        </div>

        <div class="wrap-menu-desktop">
            <nav class="limiter-menu-desktop container">

                <!-- Logo Shop -->
                <a href="#" class="logo">
                    <img src="img/logo1.png" alt="IMG-LOGO">
                </a>

                <!-- Menu desktop -->
                <div class="menu-desktop">
                    <ul class="main-menu" >
                        <li class="active-menu">
                            <a style="font-size: 22px;" href="index.html">Trang chủ</a>
                        </li>

                        <li>
                            <a style="font-size: 22px;" href="shop.html">Bộ sưu tập</a>
                            <ul class="sub-menu" >
                                <li><a style="font-size: 19px;" href="shop-gender.html">Thời trang nam</a></li>
                                <li><a style="font-size: 19px;" href="shop-gender.html">Thời trang nữ</a></li>
                                <li><a style="font-size: 19px;" href="shop-gender.html">Thời trang trẻ em</a></li>
                            </ul>
                        </li>

                        <li class="label1" data-label1="hot">
                            <a style="font-size: 22px;" href="shop-gender.html">Phụ kiện</a>
                        </li>

                        <li>
                            <a style="font-size: 22px;" href="blog.html">Tin tức</a>
                        </li>

                        <li>
                            <a style="font-size: 22px;" href="contact.html">Liên hệ</a>
                        </li>
                    </ul>
                </div>

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m">
                    <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="2">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="index.html"><img src="img/logo1.png" alt="IMG-LOGO"></a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m m-r-15">

            <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="2">
                <i class="fa fa-shopping-cart"></i>
            </div>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
        </div>
    </div>


    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="topbar-mobile">
            <li>
                <div class="left-top-bar" style="font-size: 1rem;">
                    HotLine: 0999888777
                </div>
            </li>
            <li>
                <div class="right-top-bar flex-w h-full" >
                    <a href="#" class="flex-c-m trans-04 p-lr-25" style="font-size: 1rem;">
                        <i class="fa fa-user"></i>&ensp;Đăng nhập
                    </a>

                    <a href="#" class="flex-c-m trans-04 p-lr-25" style="font-size: 1rem;">
                        <i class="fa fa-sign-out"></i>&ensp;Đăng ký
                    </a>


                </div>
            </li>
        </ul>

        <ul class="main-menu-m">
            <li>
                <a href="index.html">Trang chủ</a>
            </li>

            <li>
                <a href="shop.html">Bộ sưu tập</a>
                <ul class="sub-menu-m">
                    <li><a href="shop-gender.html">Thời trang nam</a></li>
                    <li><a href="shop-gender.html">Thời trang nữ</a></li>
                    <li><a href="shop-gender.html">Thời trang trẻ em</a></li>
                </ul>
                <span class="arrow-main-menu-m">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
					</span>
            </li>

            <li>
                <a href="shop-gender.html" class="label1 rs1" data-label1="hot">Phụ kiện</a>
            </li>

            <li>
                <a href="blog.html">Tin tức</a>
            </li>

            <li>
                <a href="contact.html">Liên hệ</a>
            </li>
        </ul>
    </div>

    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <div class="container-search-header">
            <button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
                <img src="images/icons/icon-close2.png" alt="CLOSE">
            </button>

            <form class="wrap-search-header flex-w p-l-15">
                <button class="flex-c-m trans-04">
                    <i class="zmdi zmdi-search"></i>
                </button>
                <input class="plh3" type="text" name="search" placeholder="Search...">
            </form>
        </div>
    </div>
</header>