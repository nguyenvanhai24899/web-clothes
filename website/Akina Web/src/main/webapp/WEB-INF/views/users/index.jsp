<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="${pageContext.request.contextPath}" var="base" />

<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop clothes</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
	<jsp:include page="/WEB-INF/views/users/common/css.jsp"></jsp:include>

</head>

<body>
	<%--header--%>
	<jsp:include page="/WEB-INF/views/users/common/header.jsp"></jsp:include>

    <!-- Hero Section Begin -->
    <section class="hero-section">
        <div class="hero-items owl-carousel">
            <div class="single-hero-items set-bg" data-setbg="img/bg2.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <span>Mua sắm thả ga</span>
                            <h1>Black friday</h1>
                            <p>Khuyến mại những dòng sản phẩm xin sò nhất hiện nay</p>
                            <a href="#" class="primary-btn">Mua ngay</a>
                        </div>
                    </div>
                    <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div>
                </div>
            </div>
            <div class="single-hero-items set-bg" data-setbg="img/bg6.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <span>Mua sắm thả ga</span>
                            <h1>Black friday</h1>
                            <p>Khuyến mại những dòng sản phẩm xin sò nhất hiện nay</p>
                            <a href="#" class="primary-btn">Mua ngay</a>
                        </div>
                    </div>
                    <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

	    <!-- Banner Section Begin -->
		<section class="banner spad">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 offset-lg-4">
						<div class="banner__item">
							<div class="banner__item__pic">
								<img src="img/banner/banner-1.jpg" alt="">
							</div>
							<div class="banner__item__text">
								<h2>Thời trang<br> nam</h2>
								<a href="#">Xem ngay</a>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="banner__item banner__item--middle">
							<div class="banner__item__pic">
								<img src="img/banner/banner-2.jpg" alt="">
							</div>
							<div class="banner__item__text">
								<h2>Thời trang nữ</h2>
								<a href="#">Xem ngay</a>
							</div>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="banner__item banner__item--last">
							<div class="banner__item__pic">
								<img src="img/banner/banner-3.jpg" alt="">
							</div>
							<div class="banner__item__text">
								<h2>Thời trang<br> trẻ em</h2>
								<a href="#">Xem ngay</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Banner Section End -->
<%--    ======================================list product====================================================--%>
    <div class="related-products spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Sản phẩm bán chạy</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <c:forEach items="${products}" var="product">
                    <div class="col-lg-3 col-sm-6">
                        <div class="product-item">
                            <div class="pi-pic">
<%--                                ============================images=================================--%>
                                <a href="#">
                                    <c:choose>
                                        <c:when test = "${empty product.productImages }">
                                            <img class="card-img-top" src="${base}/images/users/700x400.png" alt="">
                                        </c:when>
                                        <c:otherwise>
                                            <img class="card-img-top" width="700" height="200" src="${base}/file/upload/${product.productImages[0].path}" alt="">
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                                <div class="sale">Sale</div>
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active" ><a onclick="Shop.addItemToCart(${product.id},1)"><i class="fa fa-cart-plus"></i></a></li>
                                    <li class="quick-view"><a href="#">Xem chi tiết</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">

                                <a href="#">
                                    <h5>${product.title}</h5>
                                </a>
                                <div class="product-price">
                                    <fmt:setLocale value="vi_VN"/>
                                    <fmt:formatNumber value="${product.price}" type="currency"/>
                                    <span>
                                        <fmt:setLocale value="vi_VN"/>
										<fmt:formatNumber value="${product.price + 50000}" type="currency"/>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <!-- Hero Section End -->
    <section class="women-banner spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="product-large set-bg" data-setbg="img/products/women-large.jpg">
                        <h2>Phụ kiện</h2>
                        <a href="#">Xem thêm</a>
                    </div>
                </div>
                <div class="col-lg-8 offset-lg-1">
                    <div class="filter-control">
                        <ul>
                            <li class="active" style="font-weight: 700; font-size: 2rem;">Phụ kiện thời trang</li>
                        </ul>
                    </div>
                    <div class="product-slider owl-carousel">
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/products/women-1.jpg" alt="">
                                <div class="sale">Sale</div>
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="fa fa-cart-plus"></i></a></li>
                                    <li class="quick-view"><a href="#">Xem chi tiết</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Coat</div>
                                <a href="#">
                                    <h5>Pure Pineapple</h5>
                                </a>
                                <div class="product-price">
                                    $14.00
                                    <span>$35.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/products/women-2.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="fa fa-cart-plus"></i></a></li>
                                    <li class="quick-view"><a href="#">Xem chi tiết</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Shoes</div>
                                <a href="#">
                                    <h5>Guangzhou sweater</h5>
                                </a>
                                <div class="product-price">
                                    $13.00
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/products/women-3.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="fa fa-cart-plus"></i></a></li>
                                    <li class="quick-view"><a href="#">Xem chi tiết</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Towel</div>
                                <a href="#">
                                    <h5>Pure Pineapple</h5>
                                </a>
                                <div class="product-price">
                                    $34.00
                                </div>
                            </div>
                        </div>
                        <div class="product-item">
                            <div class="pi-pic">
                                <img src="img/products/women-4.jpg" alt="">
                                <div class="icon">
                                    <i class="icon_heart_alt"></i>
                                </div>
                                <ul>
                                    <li class="w-icon active"><a href="#"><i class="fa fa-cart-plus"></i></a></li>
                                    <li class="quick-view"><a href="#">Xem chi tiết</a></li>
                                </ul>
                            </div>
                            <div class="pi-text">
                                <div class="catagory-name">Towel</div>
                                <a href="#">
                                    <h5>Converse Shoes</h5>
                                </a>
                                <div class="product-price">
                                    $34.00
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer Section Begin -->
	<jsp:include page="/WEB-INF/views/users/common/footer.jsp"></jsp:include>
    <!-- Footer Section End -->

	<!-- java script -->
	<jsp:include page="/WEB-INF/views/users/common/js.jsp"></jsp:include>
</body>

</html>
