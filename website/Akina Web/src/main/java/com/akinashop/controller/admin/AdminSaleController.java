package com.akinashop.controller.admin;

import com.akinashop.entities.AjaxResponse;
import com.akinashop.entities.Sale;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AdminSaleController {



    RestTemplate restTemplate = new RestTemplate();
    @GetMapping(value = "/sales")
    public String sale(Model model){
        ResponseEntity<Sale[]> sales = restTemplate.getForEntity("http://localhost:8080/sales", Sale[].class);
        model.addAttribute("sales", sales.getBody());
        return "admin/sale/AdminSale";
    }

    @RequestMapping(value = "/add-sale")
    public String addSale(Model model){
        model.addAttribute("sale", new Sale());
        return "admin/sale/add-sale";
    }

    @PostMapping(value = "/save-sale")
    public String saveSale(@ModelAttribute Sale sale){
        restTemplate.postForObject("http://localhost:8080/save-sale", sale, Sale.class);
        return "redirect:/sales";
    }

    @GetMapping(value = "/delete-sale")
    public String deleteSale(@RequestParam int id){
        restTemplate.delete("http://localhost:8080/delete-sale?id="+id);
        return "redirect:/sales";
    }

    @RequestMapping(value = { "/cart" }, method = RequestMethod.GET)
    public String cart() {

        return "users/cart";
    }

    @GetMapping(value = "/code-sale")
    public String  findPriceByCode(@RequestParam String nameSale, final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        String url = "http://localhost:8080/code-sale?nameSale="+nameSale;

        Map<String, String> params = new HashMap<String, String>();
        params.put("nameSale", nameSale);
        Sale sale =  restTemplate.getForObject(url, Sale.class,params);
        model.addAttribute("sale", sale.getPriceSale());
        return  "users/cart";
    }
}
