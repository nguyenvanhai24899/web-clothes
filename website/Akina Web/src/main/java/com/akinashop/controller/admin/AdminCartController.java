package com.akinashop.controller.admin;


import com.akinashop.entities.*;
import com.akinashop.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Controller
public class AdminCartController {



    @Autowired
    ProductRepo productRepo;

    @RequestMapping(value = {"/add-to-cart"}, method = RequestMethod.POST)
    public ResponseEntity<AjaxResponse> muaHang(@RequestBody CartItem data, final ModelMap model, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        HttpSession httpSession = request.getSession();

        Cart cart = null;

        if (httpSession.getAttribute("GIO_HANG") != null) {
            cart = (Cart) httpSession.getAttribute("GIO_HANG");
        } else {
            cart = new Cart();
            httpSession.setAttribute("GIO_HANG", cart);
        }

        List<CartItem> cartItems = cart.getCartItems();
        boolean isExists = false;
        for (CartItem item : cartItems) {
            if (item.getProductId() == data.getProductId()) {
                isExists = true;
                item.setQuantity(item.getQuantity() + data.getQuantity());
            }
        }


        if (!isExists) {

            Product product = productRepo.getOne(data.getProductId());
            data.setProductName(product.getTitle());
            data.setUnitPrice(product.getPrice());
            cart.getCartItems().add(data);
        }

        long sum=0;
        for (int i=0; i<cartItems.size(); i++){
            sum += cartItems.get(i).getUnitPrice().longValue()*cartItems.get(i).getQuantity();
            httpSession.setAttribute("totalOrder", sum);
        }

         return ResponseEntity.ok(new AjaxResponse(200, String.valueOf(cart.getCartItems().size())));

    }

}