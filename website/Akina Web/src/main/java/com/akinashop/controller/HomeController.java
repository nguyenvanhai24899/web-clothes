package com.akinashop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController {


    @RequestMapping(value = { "/shop", "/shop/abc"}, method = RequestMethod.GET)
    public String shop(final ModelMap model, final HttpServletRequest request, final HttpServletResponse response)
            throws Exception {

        return "users/shop";
    }
}

