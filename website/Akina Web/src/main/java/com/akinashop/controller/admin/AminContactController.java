package com.akinashop.controller.admin;

import com.akinashop.entities.Contact;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AminContactController {
    RestTemplate restTemplate = new RestTemplate();
//    port 8081
    @RequestMapping(value = "/admin/contact")
    public String adminContact(Model model){
        ResponseEntity<Contact[]> contacts = restTemplate.getForEntity("http://localhost:8080/contact-list", Contact[].class);
        model.addAttribute("contacts", contacts.getBody());
        return "admin/AdminContact";
    }

    @RequestMapping("/contact")
    public String addContact(Model model) {
        model.addAttribute("contact", new Contact());
         return "users/contact";
    }

    @RequestMapping(value = "/delete")
    public String deleteContact(@RequestParam int id){
        restTemplate.delete("http://localhost:8080/delete?id="+id);
        return "redirect:/admin/contact";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveContact(@ModelAttribute("contact") Contact contact){
       restTemplate.postForObject("http://localhost:8080/save-contact",contact, Contact.class);
        return "redirect:/contact";
    }

}





















//    @RequestMapping("/contact")
//    public String newMess(Model model) {
//        RestTemplate restTemplate = new RestTemplate();
//        String fooResourceUrl
//                = "http://localhost:8080/admin/getContact";
//        ResponseEntity<Contact> response
//                = restTemplate.getForEntity(fooResourceUrl + "/1", Contact.class);
//       model.addAttribute("contact", response.getBody());
//        return "users/contact";
//    }
